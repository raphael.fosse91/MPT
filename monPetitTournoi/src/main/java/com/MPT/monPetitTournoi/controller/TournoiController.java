package com.MPT.monPetitTournoi.controller;

import com.MPT.monPetitTournoi.model.Tournoi;
import com.MPT.monPetitTournoi.repository.TournoiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tournois")
public class TournoiController {

    @Autowired
    private TournoiRepository tournoiRepository;

    @GetMapping("/all")
    public ResponseEntity<List<Tournoi>> recupererTournois() {
        Iterable<Tournoi> tournoisAPI = tournoiRepository.findAll();
        List<Tournoi> tournois = new ArrayList<>();
        tournoisAPI.forEach(tournois::add);

        return new ResponseEntity<>(tournois, HttpStatus.OK);
    }
}

