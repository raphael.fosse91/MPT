package com.MPT.monPetitTournoi.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "joueur")
public class Joueur {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "nom")
    private String nom;

    @Column(name = "age")
    private int age;

    @Column(name = "ville")
    private String ville;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Joueur joueur = (Joueur) o;
        return getId() != null && Objects.equals(getId(), joueur.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
