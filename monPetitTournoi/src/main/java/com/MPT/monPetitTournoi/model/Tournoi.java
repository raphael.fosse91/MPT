package com.MPT.monPetitTournoi.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "tournoi")
public class Tournoi {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "jeu")
    private String jeu;

    @Column(name = "heureDebut")
    private String heureDebut;

    @Column(name = "dateDebut")
    private String dateDebut;

    @Column(name = "joueur")
    @OneToMany
    private List<Joueur> joueurs;

    public Tournoi(UUID uuid, String jeu, String heureDebut, String dateDebut) {
        this.id = uuid;
        this.jeu = jeu;
        this.heureDebut = heureDebut;
        this.dateDebut = dateDebut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Tournoi tournoi = (Tournoi) o;
        return getId() != null && Objects.equals(getId(), tournoi.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
