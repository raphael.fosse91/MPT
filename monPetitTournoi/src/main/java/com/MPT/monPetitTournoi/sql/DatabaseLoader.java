package com.MPT.monPetitTournoi.sql;

import com.MPT.monPetitTournoi.model.Tournoi;
import com.MPT.monPetitTournoi.repository.TournoiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class DatabaseLoader implements CommandLineRunner {

    @Autowired
    private final TournoiRepository tournoiRepository;

    public DatabaseLoader(TournoiRepository tournoiRepository) {
        this.tournoiRepository = tournoiRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        this.tournoiRepository.saveAll(List.of(
                new Tournoi(UUID.randomUUID(), "TFT", "13:30", "21 juin"),
                new Tournoi(UUID.randomUUID(), "LOL", "15:30", "22 juin"),
                new Tournoi(UUID.randomUUID(), "Valorant", "16:30", "23 juin"),
                new Tournoi(UUID.randomUUID(), "Mario Kart", "16:10", "24 juin"),
                new Tournoi(UUID.randomUUID(), "Overcooked", "14:20", "25 juin"),
                new Tournoi(UUID.randomUUID(), "Street fighters", "04:30", "26 juin"),
                new Tournoi(UUID.randomUUID(), "Mario party", "14:35", "20 juin"))
        );
    }
}
