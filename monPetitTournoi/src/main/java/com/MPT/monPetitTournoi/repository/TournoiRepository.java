package com.MPT.monPetitTournoi.repository;

import com.MPT.monPetitTournoi.model.Tournoi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource
public interface TournoiRepository extends JpaRepository<Tournoi, UUID> {

}
