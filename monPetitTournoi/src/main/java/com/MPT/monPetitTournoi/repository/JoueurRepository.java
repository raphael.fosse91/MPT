package com.MPT.monPetitTournoi.repository;

import com.MPT.monPetitTournoi.model.Joueur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@RepositoryRestResource
@CrossOrigin(origins = "*")
public interface JoueurRepository extends JpaRepository<Joueur, UUID> {
}
