import {useState, useEffect} from 'react';
import Tournois from "../model/Tournois.tsx"
import Button from "../Button.tsx"
import React from 'react';

let initialTournament = [
    {id: "052e19a6-e993-11ed-a05b-0242ac120003", jeu: "TFT", heureDebut: "14:00", dateDebut: "22 juin"}
]


export function Accueil() {

    const [tournois, setTournois] = useState(initialTournament);


    const fetchData = async () => {
        const response = await fetch("api/tournois/all", {
            "headers": {
                "Content-type": "application/json"
            },
            method : "get"
        }).then(response => response.json());

        setTournois(response);

    }

    useEffect(() => {
        fetchData();
    }, [])


    return (


        <div>
        <h1>Hello world</h1>
            <ul>
                {tournois.map((tournoi, id) => (
                    <li key={`${tournoi}- ${id}`}>{tournoi.jeu}</li>
                ))}
            </ul>
        </div>


    )
}

export default Accueil