import React, { Component } from 'react';

type Tournois = {
    id: UUID;
    jeu: String;
    heureDebut: String;
    dateDebut: String;
    joueurs: List;
}

export default Tournois;
